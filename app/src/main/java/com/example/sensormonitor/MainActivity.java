package com.example.sensormonitor;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.TextView;
import android.hardware.SensorManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.util.List;

public class MainActivity extends WearableActivity implements SensorEventListener {

    private TextView xTextView;
    private TextView yTextView;
    private TextView zTextView;
    private TextView xgTextView;
    private TextView ygTextView;
    private TextView zgTextView;
    private TextView hTextView;

    private static final String TAG = "PRINT";

    private SensorManager sensorManager;
    private Sensor accel;
    private Sensor gyros;
    private Sensor heart;

    static boolean acquisition = false;

    DataOutputStream accelDOS;
    DataOutputStream gyrosDOS;
    DataOutputStream heartDOS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        xTextView  = findViewById(R.id.xTextView);
        yTextView  = findViewById(R.id.yTextView);
        zTextView  = findViewById(R.id.zTextView);
        xgTextView = findViewById(R.id.xgTextView);
        ygTextView = findViewById(R.id.ygTextView);
        zgTextView = findViewById(R.id.zgTextView);
        hTextView  = findViewById(R.id.hTextView);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        // List all available sensors with the current permissions in console logs

        List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ALL);

        for (Sensor s : sensors)
            Log.d(TAG, s.toString());

        accel = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        gyros = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        heart = sensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);

        sensorManager.registerListener(this, accel, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(this, gyros, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(this, heart, SensorManager.SENSOR_DELAY_FASTEST);

        // No calibration step in this POC (may not be needed actually)

        Log.d(TAG, "No calibration");

        // open or create and open logging file if not present

        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath() + "/Inria/BasicSensorMonitor");
        dir.mkdirs();
        File accelFile = new File(dir, "accel.log");
        File gyrosFile = new File(dir, "gyros.log");
        File heartFile = new File(dir, "heart.log");
        try {
            accelFile.createNewFile();
            gyrosFile.createNewFile();
            heartFile.createNewFile();

            FileOutputStream accelFOS = new FileOutputStream(accelFile, true);
            FileOutputStream gyrosFOS = new FileOutputStream(gyrosFile, true);
            FileOutputStream heartFOS = new FileOutputStream(heartFile, true);

            accelDOS = new DataOutputStream(accelFOS);
            gyrosDOS = new DataOutputStream(gyrosFOS);
            heartDOS = new DataOutputStream(heartFOS);
        } catch (IOException e) {
            Log.e(TAG, "IO Exception");
            e.printStackTrace();
        }

        // Enables Always-on
        setAmbientEnabled();
    }

    public void onClickStart(View v)
    {
        this.acquisition = true;
    }

    public void onClickStop(View v)
    {
        this.acquisition = false;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        switch (event.sensor.getType()) {

            case Sensor.TYPE_LINEAR_ACCELERATION:
                xTextView.setText(String.format("% .1f", event.values[0]) + " m/s²");
                yTextView.setText(String.format("% .1f", event.values[1]) + " m/s²");
                zTextView.setText(String.format("% .1f", event.values[2]) + " m/s²");
                // Stored as contiguous packets of 64 + 32*3 bits :
                // +----------------+----------+---------+---------+
                // | long timestamp | float X  | float Y | float Z |
                // +----------------+----------+---------+---------+
                if (this.acquisition) {
                    try {
                        accelDOS.writeLong(System.currentTimeMillis());
                        accelDOS.writeFloat(event.values[0]);
                        accelDOS.writeFloat(event.values[1]);
                        accelDOS.writeFloat(event.values[2]);
                    } catch (IOException e) {
                        Log.d(TAG, "IO Exception");
                        e.printStackTrace();
                    }
                }
                break;

            case Sensor.TYPE_GYROSCOPE:
                xgTextView.setText(String.format("% .1f", event.values[0]) + " rad/s");
                ygTextView.setText(String.format("% .1f", event.values[1]) + " rad/s");
                zgTextView.setText(String.format("% .1f", event.values[2]) + " rad/s");
                // Same format as above
                if (this.acquisition) {
                    try {
                        gyrosDOS.writeLong(System.currentTimeMillis());
                        gyrosDOS.writeFloat(event.values[0]);
                        gyrosDOS.writeFloat(event.values[1]);
                        gyrosDOS.writeFloat(event.values[2]);
                    } catch (IOException e) {
                        Log.d(TAG, "IO Exception");
                        e.printStackTrace();
                    }
                }
                break;

            case Sensor.TYPE_HEART_RATE:
                hTextView.setText(String.format("%.2f", event.values[0]) + " bpm");
                // Stored as contiguous packets of 64+32 bits :
                // +----------------+-----------+
                // | long timestamp | float bpm |
                // +----------------+-----------+
                if (this.acquisition) {
                    try {
                        heartDOS.writeLong(System.currentTimeMillis());
                        heartDOS.writeFloat(event.values[0]);
                    } catch (IOException e) {
                        Log.d(TAG, "IO Exception");
                        e.printStackTrace();
                    }
                }
                break;

            default:
                Log.d(TAG, "Got unexpected sensor event");
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Ignored
    }
}
