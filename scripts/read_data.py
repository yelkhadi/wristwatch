#!/usr/bin/env python3

# Example code for reading sensor logs from the Basic Sensor Monitor Wear OS App

import numpy as np
import pandas as pd


def read_heart(file):

    dt = np.dtype([('timestamp', '>i8'), ('bpm', '>f4')])
    return np.fromfile(file, dtype=dt)


def read_xyz(file):

    dt = np.dtype([('timestamp', '>i8'), ('x', '>f4'), ('y', '>f4'), ('z', '>f4')])
    return np.fromfile(file, dtype=dt)

def to_dataframe(arr):

    df = pd.DataFrame(arr.byteswap().newbyteorder()) # pandas breaks when dealing with big-endian stuff I guess
    df.set_index("timestamp", inplace=True)
    df.index = pd.to_datetime(df.index * 10**6) # timestamp is in milliseconds
    return df


if __name__ == '__main__':

    import argparse
    import os
    import sys

    description = 'Example code for reading binary sensor data produced from TicWatch'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--heart', help='Path to the file containing bpm data')
    parser.add_argument('--gyros', help='Path to the file containing gyroscopic data')
    parser.add_argument('--accel', help='Path to the file containing accelerometer data')

    args = parser.parse_args()

    exit_code = 0

    if len(sys.argv) <= 1:
        print("Nothing to do....")

    if args.heart:
        if os.path.isfile(args.heart):
            print(to_dataframe(read_heart(args.heart)))
        else:
            exit_code = 1
            print("Not a valid bpm data file", file=sys.stderr)

    if args.gyros:
        if os.path.isfile(args.gyros):
            print(to_dataframe(read_xyz(args.gyros)))
        else:
            exit_code = 1
            print("Not a valid xyz data file", file=sys.stderr)

    if args.accel:
        if os.path.isfile(args.accel):
            print(to_dataframe(read_xyz(args.accel)))
        else:
            exit_code = 1
            print("Not a valid xyz data file", file=sys.stderr)

    exit(exit_code)
